import { Component, OnInit } from '@angular/core';
import { ItensI } from '../models/itens.interface';
import { ItensserviceService } from '../services/itensservice.service';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  itens: ItensI[];
  constructor(private itensservice:ItensserviceService ){

  }

  ngOnInit(){
    this.itensservice.getItens().subscribe( res => this.itens = res );
  }

}
