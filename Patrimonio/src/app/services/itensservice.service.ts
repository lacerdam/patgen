import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection  } from 'angularfire2/firestore';
import { observable, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ItensI } from '../models/itens.interface';


@Injectable({
  providedIn: 'root'
})
export class ItensserviceService {
private itensCollection: AngularFirestoreCollection<ItensI>;
private itens: Observable<ItensI[]>;
  constructor(db: AngularFirestore) { 
    this.itensCollection = db.collection<ItensI>('itens');
    this.itens = this.itensCollection.snapshotChanges().pipe(map(
      actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id, ...data};
        })
      }
    ));
  }

  getItens(){
    return this.itens;
  }

  getItem(id:string){
    return this.itensCollection.doc<ItensI>(id).valueChanges();
  }

  updateItens(iten:ItensI, id: string){
    return this.itensCollection.doc(id).update(iten);
  }

  addIten(iten: ItensI){
    return this.itensCollection.add(iten);
  }

  removeIten(id:string){
    return this.itensCollection.doc(id).delete();
  }
}
