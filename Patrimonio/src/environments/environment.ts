// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBf4cGFDuWdXWST3ltuqhXmYSaFfMJ8Rt0",
    authDomain: "patrimonio-78e68.firebaseapp.com",
    databaseURL: "https://patrimonio-78e68.firebaseio.com",
    projectId: "patrimonio-78e68",
    storageBucket: "patrimonio-78e68.appspot.com",
    messagingSenderId: "957543157698",
    appId: "1:957543157698:web:db075fdf77babd932b905b"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
